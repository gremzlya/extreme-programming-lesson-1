import test from 'ava';
import sinon from 'sinon';

const testImportedController = {
  count(number) {
    return number;
  },
};

test('should 1 be equal 1', (t) => {
  t.is(1, 1);
});

test('should test testImportedController count method', (t) => {
  t.plan(2);

  sinon.spy(testImportedController, 'count');

  testImportedController.count(2);

  t.true(testImportedController.count.calledOnce);
  t.true(testImportedController.count.calledWith(2));
});
